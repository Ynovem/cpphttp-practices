#include <map>
#include <string>
#include <iostream>

#include <httplib.h>
#include <json.hpp>

using namespace std;
using namespace nlohmann;


int main(int argc, char* argv[])
{

	httplib::Server svr;
	map<unsigned int, string> users{
		{1, "Anikó"},
		{2, "Geri"},
	};

	svr.Get("/", [](const httplib::Request& req, httplib::Response& res) {
		cout << "get a request: /" << endl;
		res.set_content("Hello World!", "text/plain");
	});

	svr.Get("/users", [&](const httplib::Request& req, httplib::Response& res) {
		json userArray = json::array({});

		for (const auto & user : users) {
			userArray.push_back(user.second);
		}

		res.set_content(userArray.dump(), "application/json");
	});

	svr.Get("/users/(\\d+)", [&](const httplib::Request& req, httplib::Response& res) {
		auto idStr = req.matches[1];
		unsigned int id = stoul(idStr);

		auto userIt = users.find(id);

		if (userIt != users.end()) {
			json userObject = json::object({});
			userObject["id"] = userIt->first;
			userObject["name"] = userIt->second;


			res.set_content(userObject.dump(), "application/json");
		} else {
			res.status = 404;
		}
	});

	svr.Put("/users/(\\d+)", [&](const httplib::Request& req, httplib::Response& res) {
		auto idStr = req.matches[1];
		unsigned int id = stoul(idStr);
		string newName = req.body;
		auto userIt = users.find(id);

		if (userIt != users.end()) {
			res.status = 200;
			users[id] = newName;
		} else {
			res.status = 404;
		}
	});

	cout << "Server listening on: localhost:3000" << endl;
	svr.listen("localhost", 3000);
}
